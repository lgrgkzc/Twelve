package com.kzc.twelve;

import android.app.Application;
import android.util.Log;

/**
 * Created by 柯尊诚 on 2015/12/9.
 * kzc
 */
public class GameApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("kzc", "e-mail：mhkzc52@163.com");
        ApplicationHelper.init(this);
    }


}

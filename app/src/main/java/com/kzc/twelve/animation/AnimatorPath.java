package com.kzc.twelve.animation;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 路径动画
 * Created by 柯尊诚 on 2015/12/9.
 * kzc
 */
public class AnimatorPath {

    /*点的路径*/
    ArrayList<PathPoint> mPoints = new ArrayList<>();

    /*从当前路径移动到新x,y,增加一个道路*/
    public void moveTo(float x, float y) {
        mPoints.add(PathPoint.moveTo(x, y));
    }

    /* 从当前创建一条直线路径指向指定的新的x和y。*/
    public void lineTo(float x, float y) {
        mPoints.add(PathPoint.lineTo(x, y));
    }

    /*创建一个曲线路径指向新路径*/
    public void curveTo(float c0X, float c0Y, float c1X, float c1Y, float x, float y) {
        mPoints.add(PathPoint.curveTo(c0X, c0Y, c1X, c1Y, x, y));
    }

    /*返回所有点的路径*/
    public Collection<PathPoint> getPoints() {
        return mPoints;
    }


}

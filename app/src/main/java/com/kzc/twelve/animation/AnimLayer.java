package com.kzc.twelve.animation;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;

import com.kzc.twelve.Config;
import com.kzc.twelve.alg.Location;
import com.kzc.twelve.entity.Card;
import com.kzc.twelve.ui.CardView;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.animation.AnimatorProxy;

import java.util.List;

/**
 * 这是一个控制移动动画类，控制动画层
 * Created by 柯尊诚 on 2015/12/9.
 * kzc
 */
public class AnimLayer extends FrameLayout {

    private AnimatorProxy cardViewProxy;

    public AnimLayer(Context context) {
        super(context);
    }

    public AnimLayer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AnimLayer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public AnimLayer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private CardView cardView = null;

    /**
     * 初始化卡片视图
     */
    private CardView initCardView(CardView view) {
        if (cardView == null) {
            cardView = new CardView(getContext(), new Card());
            cardView.wipeBackground();
            cardView.setSelect(true);

            addView(cardView);
        }

        LayoutParams lp = new LayoutParams(Config.CARD_WIDTH, Config.CARD_WIDTH);
        lp.leftMargin = Config.CARD_WIDTH * view.getLat();
        lp.topMargin = Config.CARD_WIDTH * view.getLog();

        cardView.setLayoutParams(lp);
        cardView.setNum(view.getNum());
        cardView.setVisibility(VISIBLE);

        cardViewProxy = AnimatorProxy.wrap(cardView);

        return cardView;
    }

    /**
     * 回收卡片，删除卡片
     */
    private void recycleCard() {
        cardView.setVisibility(GONE);
        cardView.setAnimation(null);
    }

    /**
     * 创建移动动画
     */
    public void createMoveAnim(final CardView from, final CardView to, List<Location> locations) {
        if (locations.size() < 1) {
            throw new RuntimeException("没有可用行走路径！");
        }

        initCardView(from);

        ViewHelper.setPivotX(cardView, 0);
        ViewHelper.setPivotY(cardView, 0);

        AnimatorPath path = new AnimatorPath();
        int index = 0;

        int x = 0, y = 0;
        path.lineTo(x, y);

        //绘制运动轨迹
        for (int i = locations.size() - 1; i > 0; i--) {
            Location fromLoc = locations.get(i);
            Location toLoc = locations.get(i - 1);

            if (toLoc.getY() != fromLoc.getY()) {
                x += Config.CARD_WIDTH * (toLoc.getY() - fromLoc.getY());
            } else if (toLoc.getX() != fromLoc.getX()) {
                y += Config.CARD_WIDTH * (toLoc.getX() - fromLoc.getX());
            }

            path.lineTo(x, y);
        }

        ObjectAnimator anim = ObjectAnimator.ofObject(this, "cardViewLoc", new PathEvaluator(), path.getPoints().toArray());

        anim.setDuration(Config.CARD_ANIMATION_DURATION * locations.size());

        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                Log.d("xxxx", "onAnimationStart");
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                Log.d("xxxx", "onAnimationEnd");
                recycleCard();

                if (listener != null) {
                    listener.onAnimationComplete();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                Log.d("xxxx", "onAnimationCancel");
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                Log.d("xxxx", "onAnimationRepeat");
            }
        });

        anim.start();
    }

    public void setCardViewLoc(PathPoint newLoc) {
        cardViewProxy.setTranslationX(newLoc.mX);
        cardViewProxy.setTranslationY(newLoc.mY);
    }

    private AnimationListener listener;

    public void setListener(AnimationListener listener) {
        this.listener = listener;
    }

    public static interface AnimationListener {
        public void onAnimationComplete();
    }
}

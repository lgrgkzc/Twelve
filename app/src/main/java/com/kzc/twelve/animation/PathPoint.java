package com.kzc.twelve.animation;

/**
 * 路径点
 * Created by 柯尊诚 on 2015/12/9.
 * kzc
 */
public class PathPoint {

    public static final int MOVE = 0;
    public static final int LINE = 1;
    public static final int CURVE = 2;

    float mX, mY;

    float mControl0X, mControl0Y;

    float mControl1X, mControl1Y;

    int mOperation;

    private PathPoint(int operation, float x, float y) {
        mOperation = operation;
        mX = x;
        mY = y;
    }

    private PathPoint(float c0X, float c0Y, float c1X, float c1Y, float x, float y) {
        mControl0X = c0X;
        mControl0Y = c0Y;
        mControl1X = c1X;
        mControl1Y = c1Y;
        mX = x;
        mY = y;
        mOperation = CURVE;
    }

    public static PathPoint lineTo(float x, float y) {
        return new PathPoint(LINE, x, y);
    }

    public static PathPoint curveTo(float c0X, float c0Y, float c1X, float c1Y, float x, float y) {
        return new PathPoint(c0X, c0Y, c1X, c1X, x, y);
    }

    public static PathPoint moveTo(float x, float y) {
        return new PathPoint(MOVE, x, y);
    }
}

package com.kzc.twelve.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.kzc.twelve.Config;
import com.kzc.twelve.R;
import com.kzc.twelve.entity.Card;

/**
 * 卡片View
 * Created by 柯尊诚 on 2015/12/9.
 * kzc
 */
public class CardView extends FrameLayout{

    private Card card; //卡片
    private TextView label; //卡片数值
    private View background; //设置背景

    private boolean isSelect = false;

    public CardView(Context context, Card card) {
        super(context);
        this.card = card;

        init();
    }

    /**
     * 初始化视图
     */
    private void init() {
        background = new View(getContext());
        background.setBackgroundResource(R.drawable.card_bg_00);

        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        lp.setMargins(0, 0, 0, 0);

        addView(background, lp);

        label = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.card_layout, null);

        addView(label);

        reset();
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean isSelect) {
        this.isSelect = isSelect;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        reset();
        this.card = card;
        refresh();
    }

    public int getLat() {
        return card.getLat();
    }

    public int getLog() {
        return card.getLog();
    }

    /**
     * 设置卡片数值
     */
    public void setNum(int num) {
        card.setNum(num);
        refresh();
    }

    public int getNum() {
        return card.getNum();
    }

    /**
     * 判空
     */
    public boolean isEmpty() {
        return card.getNum() == 0;
    }


    /**
     * 刷新布局,显示卡片
     */
    protected void refresh() {
        if (this.getNum() <= 0) {
            label.setText("");
        } else {
            label.setText(this.card.getNum() + "");
        }

        if (isSelect) {
            label.setSelected(true);
            label.setTextSize(Config.CARD_TEXT_SELECTED);
        } else {
            label.setSelected(false);
            label.setTextSize(Config.CARD_TEXT_SIZE);
        }

        //根据卡片数值选择卡片样式
        label.setBackgroundResource(Config.CARD_LABEL_BG[getNum()]);
    }

    /**
     * 移动卡片
     */
    public void moveTo(CardView view) {
        if (view.isEmpty()) {
            view.isSelect = false;
            view.setNum(this.card.getNum());

            reset();
        } else {
            throw new RuntimeException("Card 移动异常");
        }
    }

    /**
     * 合并卡片
     */
    public boolean combine(CardView view) {
        setSelect(false);
        view.reset();

        setNum(this.card.getNum() + 1);

        return true;
    }

    /**
     * 判断是否可以合并
     */
    public boolean canCombine(CardView view) {
        return getNum() == view.getNum();
    }

    /**
     * 重置卡片
     */
    protected void reset() {
        setSelect(false);
        setNum(0);
    }

    public void wipeBackground() {
        this.background.setBackgroundResource(android.R.color.transparent);
    }

    /**
     * 设置选择监听
     */
    public void setOnSelectListener(final OnSelectListener listener) {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelect = !isSelect;

                listener.onSelect(isSelect(), CardView.this);
                refresh();
            }
        });
    }

    public static interface OnSelectListener {
        public void onSelect(boolean isSelect, CardView view);
    }

}

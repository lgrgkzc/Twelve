package com.kzc.twelve.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.kzc.twelve.Config;
import com.kzc.twelve.R;
import com.kzc.twelve.util.Properties;

import static com.kzc.twelve.Config.KEY_MAP;
import static com.kzc.twelve.Config.KEY_SCORE;
import static com.kzc.twelve.Config.level_1;
import static com.kzc.twelve.Config.level_2;
import static com.kzc.twelve.Config.level_3;
/**
 * 选择等级
 * Created by 柯尊诚 on 2015/12/11.
 * kzc
 */
public class LevelActivity extends Activity implements OnClickListener {

    private Button level1Btn;
    private Button level2Btn;
    private Button level3Btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_activity);

        initView();
    }

    private void initView() {
        level1Btn = (Button) findViewById(R.id.level1);
        level2Btn = (Button) findViewById(R.id.level2);
        level3Btn = (Button) findViewById(R.id.level3);

        level1Btn.setOnClickListener(this);
        level2Btn.setOnClickListener(this);
        level3Btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.level1 :
                //设置当前难度
                changeLevel(level_1);
                jumpGame();
                break;

            case R.id.level2 :
                changeLevel(level_2);
                jumpGame();
                break;

            case R.id.level3 :
                changeLevel(level_3);
                jumpGame();
                break;

            default:
                break;
        }
    }

    /**
     * 切换等级
     */
    private void changeLevel(int level) {
        Config.level = level;
        KEY_MAP = "map" + level;
        KEY_SCORE = "score" + level;
        Properties.getProperties().setLevel(level);
    }

    /**
     * 跳转到游戏
     */
    private void jumpGame() {
        Intent intent = new Intent(LevelActivity.this, GameActivity.class);
        startActivity(intent);
    }
}

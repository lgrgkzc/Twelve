package com.kzc.twelve.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.kzc.twelve.ApplicationHelper;
import com.kzc.twelve.ApplicationHelper.VibratorListener;
import com.kzc.twelve.Config;
import com.kzc.twelve.R;
import com.kzc.twelve.animation.AnimLayer;
import com.kzc.twelve.entity.Card;
import com.kzc.twelve.entity.Grade.OnScoreListener;
import com.kzc.twelve.util.Properties;

/**
 * Created by 柯尊诚 on 2015/12/10.
 * kzc
 */
public class GameActivity extends Activity{

    private TextView scoreField;
    private TextView bestScoreField;
    private ImageView resetBtn;
    private ImageView homeBtn;
    private TextView levelValue;
    private GameCanvas canvas;
    private AnimLayer animLayer;

    private ApplicationHelper helper = ApplicationHelper.getHelper();
    private Properties properties = Properties.getProperties();

    private int score;
    private int bestScore;

    public GameActivity() {
        helper.setScoreListener(listener);
        helper.setVibratorListener(vibratorListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        initView();
        setLevel();
        canvas.setAnimLayer(animLayer);
    }

    /**
     * 初始化
     */
    private void initView() {
        scoreField = (TextView) findViewById(R.id.score_field);
        bestScoreField = (TextView) findViewById(R.id.best_score_field);
        resetBtn = (ImageView) findViewById(R.id.reset_iv);
        homeBtn = (ImageView) findViewById(R.id.home_iv);
        levelValue = (TextView) findViewById(R.id.level_tv);
        canvas = (GameCanvas) findViewById(R.id.canvas);
        animLayer = (AnimLayer) findViewById(R.id.anim_layer);

        resetBtn.setOnClickListener(resetListener);

        homeBtn.setOnClickListener(homeListener);
    }

    /**
     * 设置显示等级
     */
    private void setLevel() {
        switch (properties.getLevel()) {
            case Config.level_1 :
                levelValue.setText("Level 1");
                break;
            case Config.level_2 :
                levelValue.setText("Level 2");
                break;
            case Config.level_3 :
                levelValue.setText("Level 3");
                break;
            default:
                break;
        }
    }


    /**
     * 点击重新开始
     */
    private OnClickListener resetListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
            builder.setTitle("是否重新开始");
            builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    canvas.initCards();
                    canvas.startGame();
                    setScore(0);
                }

            });

            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.create().show();
        }
    };

    /**
     * 点击返回主页
     */
    private OnClickListener homeListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        //获取历史数据
        setBestScore(properties.getMaxScore());
    }

    @Override
    protected void onPause() {
        super.onPause();
        //保存数据
        properties.setMap(canvas.cardsMap, Config.KEY_MAP);
    }

    /**
     * 震动监听
     */
    private VibratorListener vibratorListener = new VibratorListener() {
        @Override
        public void onVibrator() {
            //添加抖动效果
            Animation animation = AnimationUtils.loadAnimation(GameActivity.this, R.anim.shake);//加载动画资源文件
            findViewById(R.id.main_game).startAnimation(animation);//播放动画效果

            //调用震动
            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(100);
        }
    };


    /**
     * 成绩变动监听
     */
    private OnScoreListener listener = new OnScoreListener() {
        @Override
        public void rewards(int score) {
            GameActivity.this.rewards(score);
        }

        @Override
        public boolean initCompletion() {//判断是否有历史数据
            Card[][] cards = properties.getMap(Config.KEY_MAP);
            if (cards != null) {
                setScore(properties.getScore(Config.KEY_SCORE));
                canvas.onRegain(cards);
                return true;
            }
            return false;
        }

        @Override
        public void restart() {
            setScore(0);
        }
    };

    private void rewards(int score) {
        setScore(this.score + score);
        if (this.score > bestScore) {
            setBestScore(this.score);
        }
    }

    /**
     * 设置分数
     */
    public void setScore(int score) {
        this.score = score;
        scoreField.setText(String.valueOf(score));

        //保存成绩
        properties.setScore(score, Config.KEY_SCORE);
    }

    /**
     * 设置最高分数
     */
    public void setBestScore(int bestScore) {
        this.bestScore = bestScore;
        bestScoreField.setText(String.valueOf(bestScore));
        properties.setMaxScore(bestScore);
    }
}

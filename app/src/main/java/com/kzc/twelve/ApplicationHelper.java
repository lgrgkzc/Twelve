package com.kzc.twelve;

import android.app.Application;
import android.content.Context;

import com.kzc.twelve.entity.Grade.OnScoreListener;
import com.kzc.twelve.util.Properties;


/**
 * 应用帮助类,启动加载
 * Created by 柯尊诚 on 2015/12/9.
 * kzc
 */
public class ApplicationHelper {

    private static ApplicationHelper helper;


    public static ApplicationHelper init(Application application) {
        return helper = new ApplicationHelper(application);
    }

    public static ApplicationHelper getHelper() {
        return helper;
    }

    private Application application;

    public ApplicationHelper(Application application) {
        this.application = application;
        Properties.init(this.application.getSharedPreferences("twelve", Context.MODE_PRIVATE));
    }

    private Properties properties;

    public Properties getProperties() {
        return properties;
    }

    private OnScoreListener onScoreListener;

    public OnScoreListener getScoreListener() {
        return onScoreListener;
    }

    public void setScoreListener(OnScoreListener listener) {
        this.onScoreListener = listener;
    }


    /*震动监听*/
    private VibratorListener vibratorListener;

    public VibratorListener getVibratorListener() {
        return vibratorListener;
    }

    public void setVibratorListener(VibratorListener listener) {
        this.vibratorListener = listener;
    }

    public static interface VibratorListener {

        public void onVibrator();

    }
}

package com.kzc.twelve.entity;

/**
 *
 * 画布上的卡片，实体类
 * Created by 柯尊诚 on 2015/12/9.
 * kzc
 */
public class Card {

    private int lat; //x轴坐标
    private int log; //y轴坐标
    private int num = 0; //显示数字

    public Card() {

    }

    public Card(int lat, int log) {
        this.lat = lat;
        this.log = log;
    }

    public int getLat() {
        return lat;
    }

    public void setLat(int lat) {
        this.lat = lat;
    }

    public int getLog() {
        return log;
    }

    public void setLog(int log) {
        this.log = log;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}

package com.kzc.twelve.entity;

import java.util.Date;

/**
 * 记录成绩，分数，实体类
 * Created by 柯尊诚 on 2015/12/9.
 * kzc
 */
public class Grade {

    private String name; //玩家
    private Date date = new Date(); //日期
    private int baseScore; //最高分
    private int score; //当前分数

    public Grade() {

    }

    public Grade(String name, int baseScore) {
        this.name = name;
        this.baseScore = baseScore;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getBaseScore() {
        return baseScore;
    }

    public void setBaseScore(int baseScore) {
        this.baseScore = baseScore;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    /**
     * 数值越高，奖励越高
     */
    public void rewards(Card card) {
        score = score + card.getNum();
    }

    /**
     * 分数监听器
     */
    public static interface OnScoreListener {
        public void rewards(int score);

        public boolean initCompletion();

        public void restart();
    }
}

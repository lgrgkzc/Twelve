package com.kzc.twelve.util;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.util.Log;

import com.kzc.twelve.Config;
import com.kzc.twelve.entity.Card;
import com.kzc.twelve.ui.CardView;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * 用于存储
 * Created by 柯尊诚 on 2015/12/9.
 * kzc
 */
public class Properties {

    private static Properties properties;


    public static Properties getProperties() {
        return properties;
    }

    public static void init(SharedPreferences preferences) {
        properties = new Properties(preferences);
    }


    private SharedPreferences preferences;

    public Properties(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public void unregisterChangeLister(OnSharedPreferenceChangeListener listener) {
        preferences.unregisterOnSharedPreferenceChangeListener(listener);
    }


    private static final String KEY_LEVEL= "level";

    /**
     * 存储等级
     */
    public void setLevel(int level) {
        Editor editor = preferences.edit();
        editor.putInt(KEY_LEVEL, level);
        editor.commit();
    }

    /**
     * 获取等级
     * @return
     */
    public int getLevel() {
        return preferences.getInt(KEY_LEVEL, Config.level_1);
    }


    /**
     * 将卡片数值转为json，存储
     * @param card
     */
    public void setMap(CardView[][] card, String keyMap) {
        Editor editor = preferences.edit();
        JSONArray data = new JSONArray();
        for (int y = 0; y < Config.row; y++) {
            JSONArray json = new JSONArray();
            for (int x = 0; x < Config.col; x++) {
                json.put(card[x][y].getNum());
            }
            data.put(json);
        }
        editor.putString(keyMap, data.toString());
        Log.d("xxxx", data.toString());
        editor.commit();
    }

    /**
     * 获取卡片数值
     */
    public Card[][] getMap(String keyMap) {
        Card[][] cards = new Card[Config.col][Config.row];
        try {
            String mapData = preferences.getString(keyMap, "KZC");
            if (mapData == "KZC" || mapData.equals("KZC"))
                return null;
            JSONArray data = new JSONArray(mapData);
            for (int y = 0; y < Config.row; y++) {
                JSONArray json = data.getJSONArray(y);

                for (int x = 0; x < Config.col; x++) {
                    cards[x][y] = new Card(x, y);
                    cards[x][y].setNum(json.getInt(x));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cards;
    }

    private static final String KEY_MAX_SCORE = "score_max";

    /**
     * 存储最高成绩
     */
    public void setMaxScore(int score) {
        Editor e = preferences.edit();
        e.putInt(KEY_MAX_SCORE, score);
        e.commit();
    }

    /**
     * 获取最高成绩
     */
    public int getMaxScore() {
        return preferences.getInt(KEY_MAX_SCORE, 0);
    }


    /**
     * 存储成绩
     */
    public void setScore(int score, String key) {
        Editor e = preferences.edit();
        e.putInt(key, score);
        e.commit();
    }

    /**
     * 获取成绩
     */
    public int getScore(String key) {
        return preferences.getInt(key, 0);
    }


}

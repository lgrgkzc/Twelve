package com.kzc.twelve;

/**
 * 配置文件
 * Created by 柯尊诚 on 2015/12/9.
 * kzc
 */
public class Config {

    /*卡片行数*/
    public static final int row = 5;

    /*卡片列数*/
    public static final int col = 4;

    /*卡片宽度*/
    public static int CARD_WIDTH = 0;

    /*等级难度值*/
    public static final int level_1 = 4;
    public static final int level_2 = 3;
    public static final int level_3 = 2;

    /*当前难度*/
    public static int level = Config.level_1;

    /*保存卡片键值*/
    public static String KEY_MAP = "map";
    /*保存成绩键值*/
    public static String KEY_SCORE = "score";

    public static String INFO = "";

    /*卡片默认大小*/
    public static float CARD_TEXT_SIZE = 30f;
    /*卡片选中缩小后的大小*/
    public static float CARD_TEXT_SELECTED = 25f;

    public static boolean isCombine = false;
    /*卡片动画时间*/
    public static int CARD_ANIMATION_DURATION = 80;

    /*卡片背景*/
    public static int[] CARD_LABEL_BG = {
            R.drawable.card_bg_00,
            R.drawable.card_bg_01,
            R.drawable.card_bg_02,
            R.drawable.card_bg_03,
            R.drawable.card_bg_04,
            R.drawable.card_bg_05,
            R.drawable.card_bg_06,
            R.drawable.card_bg_07,
            R.drawable.card_bg_08,
            R.drawable.card_bg_09,
            R.drawable.card_bg_10,
            R.drawable.card_bg_11,
            R.drawable.card_bg_12,
            R.drawable.card_bg_13,
            R.drawable.card_bg_14,
            R.drawable.card_bg_15,
            R.drawable.card_bg_16,
            R.drawable.card_bg_17,
            R.drawable.card_bg_18,
            R.drawable.card_bg_19,
            R.drawable.card_bg_20,
    };


}

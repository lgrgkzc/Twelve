package com.kzc.twelve.alg;

/**
 * 轨迹动画，点位置对象
 * Created by 柯尊诚 on 2015/12/9.
 * kzc
 */
public class Location {

    private int x;
    private int y;
    private int movedSteps; //移动步骤
    private int evalRemainSteps; //真实步骤
    private int totalEvalSteps; //总步骤
    private Location previous;

    public Location(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean equals(Object o) {
        if (o instanceof Location) {
           Location l = (Location) o;
            if (l.x == this.x && l.y == this.y) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "[" + x + ", " + y + ']';
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getMovedSteps() {
        return movedSteps;
    }

    public void setMovedSteps(int movedSteps) {
        this.movedSteps = movedSteps;
    }

    public int getEvalRemainSteps() {
        return evalRemainSteps;
    }

    public void setEvalRemainSteps(int evalRemainSteps) {
        this.evalRemainSteps = evalRemainSteps;
    }

    public int getTotalEvalSteps() {
        return totalEvalSteps;
    }

    public void setTotalEvalSteps(int totalEvalSteps) {
        this.totalEvalSteps = totalEvalSteps;
    }

    public Location getPrevious() {
        return previous;
    }

    public void setPrevious(Location previous) {
        this.previous = previous;
    }
}

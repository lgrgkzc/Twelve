/*******************************************************************************
 * Copyright (c) 2015. iMeth (http://www.imeth.cn). All rights reserved.
 ******************************************************************************/
package com.nineoldandroids.util;

/**
 * Thrown when code requests a {@link Property} on a class that does
 * not expose the appropriate method or field.
 *
 * @see Property#of(Class, Class, String)
 */
public class NoSuchPropertyException extends RuntimeException {

    public NoSuchPropertyException(String s) {
        super(s);
    }

}
